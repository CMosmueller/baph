/* global checkEmpty, room, password, settings */
document.getElementById('sendmail').onclick = function () {
  if (checkEmpty(room.value, password.value)) {
    $.post('/php/sendMail.php', {
      lang: document.documentElement.lang,
      room: room.value,
      password: password.value,
      email: document.getElementById('invite').value
    }, function(data, status) {
      if (data === 'success') {
        alert(settings.lang.mailFeedback.success);
      } else {
        console.error(data);
        alert(settings.lang.mailFeedback.error);
      }
    }, 'text');
  }
};
