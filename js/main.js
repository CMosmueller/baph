/* global io, RTCPeerConnection, RTCIceCandidate, RTCSessionDescription */

var localVideo = document.getElementById('localVideo');
var remoteVideo = document.getElementById('remoteVideo');
var room = document.getElementById('room');
var password = document.getElementById('password');
var submit = document.getElementById('submit');
var pwEye = document.getElementById('pw_eye');
var nutzungBCheckbox = document.getElementById('nutzungBCheckbox');

// streamsettings
var audioSupport = document.getElementById('audioSupport');
var audioSettings = document.getElementById('audioSettings');
var echoCancellation = document.getElementById("echoCancellation");
var videoSupport = document.getElementById('videoSupport');
var videoSettings = document.getElementById('videoSettings');
var frameRate = document.getElementById("frameRate");
var resolutions = document.getElementById("resolutions");

var socket;
var pc;
var localStream;
var remoteStream;
var infos = {};
var myStatus = {
  isCreator: false,
  isInRoom: false,
  hasMedia: false,
  isStarted: false,
  isInitiator: false,
  turnReady: false,
  gotParams: false
};
var settings = {
  pcConfig: {
    iceServers: [{
      // url: "stun:stun.l.google.com:19302"
      urls: [
        "stun:baph.sz-ybbs.ac.at:45000",
        "stun:stun.l.google.com:19302"
      ]
    }, {
      urls: "turn:baph.sz-ybbs.ac.at:45000",
      username: "test",
      credential: "test"
    }]
  },
  constraints: {
    audio: {
      echoCancellation: true
    },
    video: {
      width: 640,
      height: 480,
      frameRate: 12
    }
  },
  limits: {
    framerate: {
      min: 10,
      max: 30
    }
  },
  lang: {
    checkEmpty: {
      bothEmpty: 'Please choose a room name and a password.',
      roomEmpty: 'Please choose a room name.',
      passwordEmpty: 'Please choose a password.',
      eitherEmpty: 'Either the rooom or the password field is empty.'
    },
    reloadPlease: 'Something bad happened. Please reload.',
    mediaFeedback: {
      AbortError: 'Something bad happened. Please reload.',
      NotAllowedError: 'Please grant access to your camera and/or microphone. ' +
        'Our service needs them to work. ' +
        'If there is not a popup to ask you for it then please enable acces to them in your browser settings.',
      NotFoundError: 'We have not found either your camera or your microphone. ' +
        'Please check whether they are functioning.',
      NotReadableError: 'Even though you gave us access to your devices, ' +
        'we are unable to get the media due to some errors occurred at the operating system, browser or web page level.',
      OverConstrainedError: 'Something is wrong with the settings. Are you sure you aren\'t trying to hack?',
      SecurityError: 'You have disabled WebRTC. We use WebRTC in order to connect you to the other device.',
      TypeError: 'Come on. You cannot expect us to connect you to the other device when there are not any sharable medias.',
      default: 'Unknown error while trying to get camera or microphone. Please reload.'
    },
    descriptionFeedback: {
      InvalidStateError: 'There is no established connection. Maybe it got closed for some reason.',
      InvalidSessionDescriptionError: 'The session description is invalid.',
      default: 'Cannot set description.'
    },
    addIceCandidateFeedback: {
      TypeError: 'The specified candidate does not have values for both sdpMid and sdpMLineIndex.',
      InvalidStateError: 'There are no connected remote client.',
      OperationError: 'Either the sdpMid value does not match the norm or ufrag or sdpMLineIndex is too high.',
      default: 'addIceCandidate() error: '
    },
    createAnswerFeedback: {
      NotReadableError: 'The identity provider was not able to provide an identity assertion.',
      OperationError: 'Generation of the SDP failed for some reason.',
      default: 'Failed to create session description: '
    },
    mailFeedback: {
      success: 'EMail successfully send. Please check your junk mail folder if you did not receive it.',
      error: 'Unable to send mail for unknows reason.'
    }
  },
  standard: {
    audio: {
      echoCancellation: true
    },
    video: {
      width: 640,
      height: 480,
      frameRate: 12
    },
    resolutions: {
      FULLHD: {
        width: 1920,
        height: 1080
      },
      HD: {
        width: 1280,
        height: 720
      },
      LD: {
        width: 640,
        height: 480
      },
      VHS: {
        width: 320,
        height: 240
      },
      Youtube_144p: {
        width: 256,
        height: 144
      },
    },
    lang: {
      en: {
        checkEmpty: {
          bothEmpty: 'Please choose a room name and a password.',
          roomEmpty: 'Please choose a room name.',
          passwordEmpty: 'Please choose a password.',
          eitherEmpty: 'Either the rooom or the password field is empty.'
        },
        reloadPlease: 'Something bad happened. Please reload.',
        mediaFeedback: {
          AbortError: 'Something bad happened. Please reload.',
          NotAllowedError: 'Please grant access to your camera and/or microphone. ' +
            'Our service needs them to work. ' +
            'If there is not a popup to ask you for it then please enable acces to them in your browser settings.',
          NotFoundError: 'We have not found either your camera or your microphone. ' +
            'Please check whether they are functioning.',
          NotReadableError: 'Even though you gave us access to your devices, ' +
            'we are unable to get the media due to some errors occurred at the operating system, browser or web page level.',
          OverConstrainedError: 'Something is wrong with the settings. Are you sure you aren\'t trying to hack?',
          SecurityError: 'You have disabled WebRTC. We use WebRTC in order to connect you to the other device.',
          TypeError: 'Come on. You cannot expect us to connect you to the other device when there are not any sharable medias.',
          default: 'Unknown error while trying to get camera or microphone. Please reload.'
        },
        descriptionFeedback: {
          InvalidStateError: 'There is no established connection. Maybe it got closed for some reason.',
          InvalidSessionDescriptionError: 'The session description is invalid.',
          default: 'Cannot set description.'
        },
        addIceCandidateFeedback: {
          TypeError: 'The specified candidate does not have values for both sdpMid and sdpMLineIndex.',
          InvalidStateError: 'There are no connected remote client.',
          OperationError: 'Either the sdpMid value does not match the norm or ufrag or sdpMLineIndex is too high.',
          default: 'addIceCandidate() error: '
        },
        createAnswerFeedback: {
          NotReadableError: 'The identity provider was not able to provide an identity assertion.',
          OperationError: 'Generation of the SDP failed for some reason.',
          default: 'Failed to create session description: '
        },
        mailFeedback: {
          success: 'EMail successfully send. Please check your junk mail folder if you did not receive it.',
          error: 'Unable to send mail for unknows reason.'
        }
      },
      de: {
        checkEmpty: {
          bothEmpty: 'Bitte wählen Sie einen Raumnamen und ein Passwort',
          roomEmpty: 'Bitte wählen sie einen Raumnamen.',
          passwordEmpty: 'Bitte wählen sie ein Passwort.',
          eitherEmpty: 'Entweder ist das Raum- oder das Passwortfeld leer.'
        },
        reloadPlease: 'Etwas schlechtes wurde passiert. Bitte neu laden.',
        mediaFeedback: {
          AbortError: 'Etwas schlechtes wurde passiert. Bitte neu laden.',
          NotAllowedError: 'Bitte geben Sie ihr Kamera und/oder Mokrifon frei. ' +
            'Unser Dient braucht es um überhaupt funktionieren zu können. ' +
            'Falls Sie keinen Popup über die Anfrage über die Verwendung von Kamera oder Mikrofon gesehen haben geben Sie es bitte in ihren Browsereinstellungen frei.',
          NotFoundError: 'Wir haben ihr Kamera oder Mikrofon nicht gefunden. ' +
            'Bitte überprüfen Sie, ob diese funktionieren.',
          NotReadableError: 'Obwohl Sie uns ihre Mediengeräte freigegeben haben, ' +
            'können wir nicht darauf zugreifen. ' +
            'Anscheinend gibt es Fehler auf der Betriebssystem-, Browser- oder Webseiten-Level.',
          OverConstrainedError: 'Etwas stimmt mit den Einstellungen nicht. Sind Sie sicher Sie sind gerade nicht beim Hacken?',
          SecurityError: 'Sie haben WebRTC deaktiviert. Wir verwenden WebRTC um Sie mit Ihren zweiten Gerät zu verbinden.',
          TypeError: 'Nageh. Wir können Ihnen nicht mit dem zweiten Gerät verbinden wenn kein Video und Audio übertragen werden.',
          default: 'Unbekannter Fehler beim Zugreifen auf Kamera oder Mikrofon. Bitte neu laden.'
        },
        descriptionFeedback: {
          InvalidStateError: 'Es gibt keine Verbindung. Vielleicht wurde dieser aus unbekannten Gründen getrennt.',
          InvalidSessionDescriptionError: 'Die Sessionbeschreibung ist ungültig.',
          default: 'Kann die Sessionbeschreibung nicht setzen.'
        },
        addIceCandidateFeedback: {
          TypeError: 'Der angegebene Kandidat hat keine Werte für sdpMid und sdpMLineIndex.',
          InvalidStateError: 'Es gibt keine verbundene RemoteClient.',
          OperationError: 'Entweder ist der Wert von sdpMid ungültig oder ufrag oder sdpMLineIndex ist zu hoch.',
          default: 'addIceCandidate() error: '
        },
        createAnswerFeedback: {
          NotReadableError: 'Der Identitätsanbieter ist nicht in der Lage, seinen Job zu erfüllen.',
          OperationError: 'Erzeugen von SDP fehlgeschlagen.',
          default: 'Failed to create session description: '
        },
        mailFeedback: {
          success: 'Der EMail wurde gesendet. Bitte schauen Sie in Ihren Junk-Ordner falls Sie es nicht bekommen haben.',
          error: 'Der Mail konnte aus unbekannte Gründe nicht gesendet werden.'
        }
      },
      zh: {
        checkEmpty: {
          bothEmpty: '請您輸入房間名稱和密碼。',
          roomEmpty: '請您輸入房間名稱。',
          passwordEmpty: '請您輸入密碼。',
          eitherEmpty: '房間或者密碼表格沒有填.'
        },
        reloadPlease: '不詳的事情發生事情了。請您刷新網頁。',
        mediaFeedback: {
          AbortError: '不詳的事情發生事情了。請您刷新網頁。',
          NotAllowedError: '請您允許使用相機或者麥克風。' +
            '我們的服務需要。' +
            '如果沒有彈出窗口訪問使用的話請您查看您的服務器設定。',
          NotFoundError: '我們沒有發現相機或者麥克風。' +
            '請調查他們的功能。',
          NotReadableError: '即使您讓我們訪問您的設備， ' +
            '我們無法得到媒體。可能在操作系統，瀏覽器或網頁級別發生了一些錯誤。',
          OverConstrainedError: '設置有問題。您確定你沒有試圖黑客嗎？',
          SecurityError: '您禁用了WebRTC. 爲了能夠鏈接另一個設備我們需要WebRTC。',
          TypeError: '如果沒有沒有任何可共享的媒體的話，您不能指望我們將您連接到其他設備。',
          default: '發生了不明錯誤。請您刷新網頁。'
        },
        descriptionFeedback: {
          InvalidStateError: '沒有建立的連接。也許由於某些原因關閉了。',
          InvalidSessionDescriptionError: 'SESSION描述無效。',
          default: '無法設定SESSION描述。'
        },
        addIceCandidateFeedback: {
          TypeError: '指定的候選沒有sdpMid和sdpMLineIndex的值。',
          InvalidStateError: '沒有連接的遠程客戶端。',
          OperationError: 'sdpMid的值無效或者ufrag或者sdpMLineIndex的值太高。',
          default: 'addIceCandidate() error: '
        },
        createAnswerFeedback: {
          NotReadableError: '身份提供者無法提供身份聲明。',
          OperationError: '無法生成SDP。',
          default: '無法生成SESSION描述: '
        },
        mailFeedback: {
          success: '郵件以發送。諾您沒有得到的話請您查看垃圾郵件箱。',
          error: '無法發送郵件。'
        }
      }
    }
  }
};

(function initialSetup() {
  settings.lang = (document.documentElement.lang !== 'en') ? settings.standard.lang[document.documentElement.lang] : settings.lang;
  document.getElementById('stream').style.display = "none";
  submit.disabled = !nutzungBCheckbox.checked;

  // get settings ready
  (function initSettings() {
    // audio settings
    audioSupport.checked = (settings.standard.audio !== false) ? (true) : false;
    echoCancellation.checked = settings.standard.audio.echoCancellation || true;

    // video settings
    videoSupport.checked = (settings.standard.video !== false) ? (true) : false;
    frameRate.value = settings.standard.video.frameRate;
    frameRate.min = settings.limits.framerate.min;
    frameRate.max = settings.limits.framerate.max;
    frameRate.onkeypress = function() {
      return false;
    };
    // parse resolution options
    var width = document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth; // || window.innerWidth
    var height = document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;
    for (var res in settings.standard.resolutions) {
      if (settings.standard.resolutions.hasOwnProperty(res)) {
        if (width >= settings.standard.resolutions[res].width && height >= settings.standard.resolutions[res].height) {
          var resolution = document.createElement('option');
          resolution.value = res;
          var content = document.createTextNode(res + '(' +
            settings.standard.resolutions[res].width + 'x' +
            settings.standard.resolutions[res].height + ')');
          resolution.appendChild(content);
          resolutions.appendChild(resolution);
        }
      }
    }
    resolutions.value = (width >= 640) ? 'LD' :
      (width >= 320) ? 'VHS' : 'Youtube_144p';

    audioSupport.onchange = function() {
      if (audioSupport.checked) {
        audioSettings.style.display = "block";
      } else {
        audioSettings.style.display = "none";
      }
    };
    videoSupport.onchange = function() {
      if (videoSupport.checked) {
        videoSettings.style.display = "block";
      } else {
        videoSettings.style.display = "none";
      }
    };
  })();

  // get room from url or ask for it
  (function initParams() {
    var getParams = readUrlParams(window.location.href);
    // console.log('Parameters: ' + getParams);
    if (getParams && getParams.room && getParams.password) {
      myStatus.gotParams = true;
      room.value = getParams.room;
      password.value = getParams.password;

      // socket.emit('join', {
      //   roomName: room.value,
      //   pwd: password.value
      // });
      media('create or join', {
        roomName: room.value,
        pwd: password.value
      });
    }

    function readUrlParams(url) {
      var queryStart = url.indexOf("?") + 1,
        queryEnd = url.indexOf("#") + 1 || url.length + 1,
        query = url.slice(queryStart, queryEnd - 1),
        pairs = query.replace(/\+/g, " ").split("&"),
        parms = {},
        i,
        n,
        v,
        nv;

      if (query === url || query === "")
        return;

      for (i = 0; i < pairs.length; i++) {
        nv = pairs[i].split("=", 2);
        n = decodeURIComponent(nv[0]);
        v = decodeURIComponent(nv[1]);

        if (!parms.hasOwnProperty(n))
          parms[n] = [];
        parms[n].push(nv.length === 2 ? v : null);
      }
      return parms;
    }
  })();
})();

submit.onclick = function() {
  if (checkEmpty(room.value, password.value)) {
    settings.constraints = getSettings();
    media('create or join', {
      roomName: room.value,
      pwd: password.value
    });
  }
};

room.onkeypress = password.onkeypress = function(event) {
  if (event.keyCode === 13) {
    submit.onclick();
    return false;
  }
};
pwEye.onclick = function() {
  if (password.getAttribute('type') === 'text') {
    password.setAttribute('type', 'password');
    $(pwEye).addClass('glyphicon-eye-open').removeClass('glyphicon-eye-close');
  } else if (password.getAttribute('type') === 'password') {
    password.setAttribute('type', 'text');
    $(pwEye).addClass('glyphicon-eye-close').removeClass('glyphicon-eye-open');
  }
};

nutzungBCheckbox.onchange = function () {
  submit.disabled = !nutzungBCheckbox.checked;
};

document.getElementById('hangup').onclick = function() {
  toggleStreamMode();
  socket.emit('leave', {
    room: infos.room
  });
  myStatus.isStarted = false;
  myStatus.gotParams = false;
  myStatus.isCreator = false;
  myStatus.isInRoom = false;
  myStatus.isInitiator = false;

  pc.close();
  // pc = null;
};

/**
 * @description checks whether the room and password exists
 *
 * @param {string} roomId
 * @param {string} pwd
 * @returns {boolean}

 *
 */
function checkEmpty(roomId, pwd) {
  var check = false;
  if (roomId === '' || pwd === '') {
    if (roomId === '' && pwd === '') {
      alert(settings.lang.checkEmpty.bothEmpty);
    } else if (roomId === '') {
      alert(settings.lang.checkEmpty.roomEmpty);
    } else if (pwd === '') {
      alert(settings.lang.checkEmpty.passwordEmpty);
    }
    check = false;
  } else {
    check = true;
  }
  return check;
}

/**
 * @description gets the settings from html-input
 *
 * @returns {object}
 */
function getSettings() {
  var constraints = {};
  if (audioSupport.checked === true) {
    constraints.audio = true;
    if (echoCancellation.checked === true) {
      constraints.audio = {};
      constraints.audio.echoCancellation = echoCancellation.checked;
    }
  } else {
    constraints.audio = false;
  }
  // checking for video
  if (videoSupport.checked === true) {
    constraints.video = {};
    // max 24fps
    constraints.video.frameRate = (settings.limits.framerate.min <= parseInt(frameRate.value) <= settings.limits.framerate.max) ?
      parseInt(frameRate.value) : settings.standard.video.frameRate;
    // getting resolution
    var value = resolutions.options[resolutions.selectedIndex].value;
    constraints.video.width = settings.standard.resolutions[value].width;
    constraints.video.height = settings.standard.resolutions[value].height;
  } else {
    constraints.video = false;
  }
  return constraints;
}

/**
 * @description start chat if in the status for it
 *
 * @returns {void}
 */
function start() {
  if (myStatus.isInRoom && myStatus.hasMedia && !myStatus.isStarted && typeof localStream !== 'undefined') { // check if ready for webrtc
    socket.emit('webrtc', { // ask other members to start too
      room: infos.room
    });
    console.log('Start WebRTC');

    // initializing the own peer
    createPeerConnection();
    pc.addStream(localStream);
    myStatus.isStarted = true;

    if (myStatus.isInitiator) { // if I am the one who joined then call others
      console.log('Sending offer to peer');
      pc.createOffer(
        // {
        // mandatory: {
        //   OfferToReceiveAudio: true,
        //   OfferToReceiveVideo: true
        // }
        // }
      ).then(setLocalAndSendMessage).catch(function(error) {
        console.error('createOffer() error: ', error);
      });
    }
  } else {
    console.warn('Failed to start: ', myStatus);
  }
}

/**
 * @description creates a finished RTCPeerConnection
 *
 * @returns {void}
 */
function createPeerConnection() {
  try {
    pc = new RTCPeerConnection(settings.pcConfig);
    // assigning event handlers
    pc.onicecandidate = function(event) { // sends the candidates informations to everyone in the room
      if (event.candidate) {
        socket.emit('message', [{
          room: infos.room,
          tp: 'icecandidate'
        }, {
          type: 'candidate',
          label: event.candidate.sdpMLineIndex,
          id: event.candidate.sdpMid,
          candidate: event.candidate.candidate
        }]);
      }
    };
    pc.onaddstream = function(event) { // when gets a remote stream
      console.log('Remote stream added.');
      remoteStream = event.stream;
      remoteVideo.srcObject = event.stream;
    };
    pc.onremovestream = function(event) { // when a remote stream gets removed
      console.log('Remote stream removed. Event: ', event);
    };
    pc.oniceconnectionstatechange = function(event) { // wheb the remote peer changes state, be it network or whatever
      if (pc.iceConnectionState === 'disconnected') {
        // console.log('Disconnected:', event);
      } else if (event.type === 'iceconnectionstatechange') {
        // ...
      } else {
        console.warn('Disconnect event fired for unknown reasons.', event);
      }
    };
    // console.log('Created RTCPeerConnection', pc);
  } catch (e) {
    console.error('Failed to create PeerConnection, exception: ' + e.message);
    alert(settings.lang.reloadPlease);
  }
}

/**
 * @description sets the session description and broadcasts it to everyone else
 *
 * @param {sessionDescription} sessionDescription
 * @returns {void}
 */
function setLocalAndSendMessage(sessionDescription) {
  // Set Opus as the preferred codec in SDP if Opus is present.
  sessionDescription.sdp = preferOpus(sessionDescription.sdp);
  pc.setLocalDescription(new RTCSessionDescription(sessionDescription)).then(function() {
    // console.log('local description set, now sending description message');
    socket.emit('message', [{
      room: infos.room,
      tp: 'sessionDescription'
    }, sessionDescription]);
  }).catch(descriptionErrorHandling);
}

/**
 * @description getusermedia and inits and enters room and starts chat if possible
 *
 * @param {string} socketEvent
 * @param {object} socketEventParams
 * @returns {void}
 */
function media(socketEvent, socketEventParams) {
  navigator.mediaDevices
    .getUserMedia(settings.constraints)
    .then(gotStream)
    .catch(function(e) {
      switch (e.name) {
        case 'AbortError':
          {
            console.error(settings.lang.mediaFeedback.AbortError, e);
            alert(settings.lang.mediaFeedback.AbortError);
            break;
          }
        case 'NotAllowedError':
          {
            console.error(settings.lang.mediaFeedback.NotAllowedError, e);
            alert(settings.lang.mediaFeedback.NotAllowedError);
            break;
          }
        case 'NotFoundError':
          {
            console.error(settings.lang.mediaFeedback.NotFoundError, e);
            alert(settings.lang.mediaFeedback.NotFoundError);
            break;
          }
        case 'NotReadableError':
          {
            console.error(settings.lang.mediaFeedback.NotReadableError, e);
            alert(settings.lang.mediaFeedback.NotReadableError);
            break;
          }
        case 'OverConstrainedError':
          {
            console.error(settings.lang.mediaFeedback.OverConstrainedError, e);
            alert(settings.lang.mediaFeedback.OverConstrainedError);
            break;
          }
        case 'SecurityError':
          {
            console.error(settings.lang.mediaFeedback.SecurityError, e);
            alert(settings.lang.mediaFeedback.SecurityError);
            break;
          }
        case 'TypeError':
          {
            console.error(settings.lang.mediaFeedback.TypeError, e);
            alert(settings.lang.mediaFeedback.TypeError);
            break;
          }

        default:
          {
            console.error(settings.lang.mediaFeedback.default, e);
            alert(settings.lang.mediaFeedback.default);
            break;
          }
      }
    });

  function gotStream(stream) {
    socket = initSocket(socket);
    console.log('Adding local stream.');
    localVideo.volume = 0;
    localVideo.muted = true;
    localStream = stream;
    localVideo.srcObject = stream;
    myStatus.hasMedia = true;

    socket.emit(socketEvent, socketEventParams);

    if (myStatus.isInitiator) {
      start();
    }
  }
}

/**
 * @description enters or exits streaming mode
 *
 * @returns {void}
 */
function toggleStreamMode() {
  document.getElementById('stream').style.display = (document.getElementById('stream').style.display !== "none") ? "none" : "block";
  document.getElementById('inp').style.display = (document.getElementById('inp').style.display !== "none") ? "none" : "block";
  document.getElementById('settings').style.display = (document.getElementById('settings').style.display !== "none") ? "none" : "block";
  // document.getElementById('videos').style.display = "block";
  // document.getElementById('inp').style.display = "none";
  // document.getElementById('settings').style.display = "none";
}

/**
 * @description handling errors from setting descriptions
 *
 * @param {object} e
 * @returns {void}
 */
function descriptionErrorHandling(e) {
  switch (e.name) {
    case 'InvalidStateError':
      {
        console.error(settings.lang.descriptionFeedback.InvalidStateError, e);
        alert(settings.lang.descriptionFeedback.InvalidStateError);
        break;
      }
    case 'InvalidSessionDescriptionError':
      {
        console.error(settings.lang.descriptionFeedback.InvalidSessionDescriptionError, e);
        break;
      }

    default:
      {
        console.error(settings.lang.descriptionFeedback.default, e);
        break;
      }
  }
}

/**
 * @description inits the given socket
 *
 * @param {any} lsocket
 * @returns {socketIOCliet.socket}
 */
function initSocket(lsocket) {
  lsocket = io.connect('https://baph.sz-ybbs.ac.at:44000', {
    secure: true
  });

  // events
  lsocket.on('init', function(obj) {
    console.log('Initializing the informations.');
    infos = obj;
  });
  lsocket.on('log', function(message) {
    alert(message);
  });

  lsocket.on('created', function(obj) {
    console.log('Created the room ' + obj.roomName + '.');
    infos.room = obj.roomName;
    myStatus.isCreator = true;
    myStatus.isInRoom = true;
    toggleStreamMode();
    start();
  });
  lsocket.on('joined', function(obj) {
    console.log('Joined the room ' + obj.roomName + '.');
    infos.room = obj.roomName;
    myStatus.isInRoom = true;
    myStatus.isInitiator = true;
    toggleStreamMode();
    start();
  });
  lsocket.on('room does not exist', function(obj) {
    lsocket.emit('create', obj);
  });

  lsocket.on('start chat', function() {
    start();
  });
  lsocket.on('msg', function(msg) {
    switch (msg.type) {
      case 'candidate':
        {
          // {
          //   sdpMLineIndex: msg.label,
          //   candidate: msg.candidate
          // }
          if (myStatus.isStarted) {
            // console.log(new RTCIceCandidate(msg));
            pc.addIceCandidate(new RTCIceCandidate({
              sdpMLineIndex: msg.label,
              candidate: msg.candidate
            })).then(function() {
              // console.log('Ice candidate successfully added.');
            }).catch(function(error) {
              switch (error.name) {
                case 'TypeError':
                  {
                    console.error(settings.lang.addIceCandidateFeedback.TypeError, error);
                    break;
                  }
                case 'InvalidStateError':
                  {
                    console.log(settings.lang.addIceCandidateFeedback.InvalidStateError, error);
                    alert(settings.lang.addIceCandidateFeedback.InvalidStateError);
                    break;
                  }
                case 'OperationError':
                  {
                    console.error(settings.lang.addIceCandidateFeedback.OperationError, error);
                    break;
                  }
                default:
                  {
                    console.error(settings.lang.addIceCandidateFeedback.default, error);
                    break;
                  }
              }
            });
          } else {
            console.warn('Received candidates even though not started.');
          }
          break;
        }
      case 'offer':
        {
          if (!myStatus.isInitiator && !myStatus.isStarted) start();
          pc.setRemoteDescription(new RTCSessionDescription(msg)).then(function() {
            // console.log('Sending answer to peer.');
            pc.createAnswer(
              // {
              // mandatory: {
              //   OfferToReceiveAudio: true,
              //   OfferToReceiveVideo: true
              // }
              // }
            ).then(setLocalAndSendMessage).catch(function(error) {
              switch (error.name) {
                case 'NotReadableError':
                  {
                    console.error(settings.lang.createAnswerFeedback.NotReadableError, error);
                    break;
                  }
                case 'OperationError':
                  {
                    console.error(settings.lang.createAnswerFeedback.OperationError, error);
                    break;
                  }

                default:
                  {
                    console.trace(settings.lang.createAnswerFeedback.default, error);
                    break;
                  }
              }
            });
          }).catch(descriptionErrorHandling);
          break;
        }
      case 'answer':
        {
          if (myStatus.isStarted) {
            pc.setRemoteDescription(new RTCSessionDescription(msg)).then(function() {
              // console.log('Remotedescription of answer set');
            }).catch(descriptionErrorHandling);
          } else {
            console.warn('Received answer even though not started.');
          }
          break;
        }

      default:
        {
          console.warn('The type does not fit anything:', msg.type);
          break;
        }
    }
  });

  lsocket.on('disconnect', function() {
    //do stuff
  });

  return lsocket;
}

/**
 * @description Set Opus as the default audio codec if it's present.
 *
 * @param {sessionDescription.sdp} sdp
 * @returns {sessionDescription.sdp}
 */
function preferOpus(sdp) {
  var sdpLines = sdp.split('\r\n');
  var mLineIndex;
  // Search for m line.
  for (var i = 0; i < sdpLines.length; i++) {
    if (sdpLines[i].search('m=audio') !== -1) {
      mLineIndex = i;
      break;
    }
  }
  if (mLineIndex === null) {
    return sdp;
  }

  // If Opus is available, set it as the default in m line.
  for (i = 0; i < sdpLines.length; i++) {
    if (sdpLines[i].search('opus/48000') !== -1) {
      var opusPayload = extractSdp(sdpLines[i], /:(\d+) opus\/48000/i);
      if (opusPayload) {
        sdpLines[mLineIndex] = setDefaultCodec(sdpLines[mLineIndex], opusPayload);
      }
      break;
    }
  }

  // Remove CN in m line and sdp.
  sdpLines = removeCN(sdpLines, mLineIndex);

  sdp = sdpLines.join('\r\n');
  return sdp;

  /******* */
  function extractSdp(sdpLine, pattern) {
    var result = sdpLine.match(pattern);
    return result && result.length === 2 ? result[1] : null;
  }

  // Set the selected codec to the first in m line.
  function setDefaultCodec(mLine, payload) {
    var elements = mLine.split(' ');
    var newLine = [];
    var index = 0;
    for (var i = 0; i < elements.length; i++) {
      if (index === 3) { // Format of media starts from the fourth.
        newLine[index++] = payload; // Put target payload to the first.
      }
      if (elements[i] !== payload) {
        newLine[index++] = elements[i];
      }
    }
    return newLine.join(' ');
  }

  // Strip CN from sdp before CN constraints is ready.
  function removeCN(sdpLines, mLineIndex) {
    var mLineElements = sdpLines[mLineIndex].split(' ');
    // Scan from end for the convenience of removing an item.
    for (var i = sdpLines.length - 1; i >= 0; i--) {
      var payload = extractSdp(sdpLines[i], /a=rtpmap:(\d+) CN\/\d+/i);
      if (payload) {
        var cnPos = mLineElements.indexOf(payload);
        if (cnPos !== -1) {
          // Remove CN payload from m line.
          mLineElements.splice(cnPos, 1);
        }
        // Remove CN line in sdp
        sdpLines.splice(i, 1);
      }
    }

    sdpLines[mLineIndex] = mLineElements.join(' ');
    return sdpLines;
  }
}
