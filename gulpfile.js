//jshint node:true, esversion:6
//jshint browser:false

//including gulp
var gulp = require('gulp');

//including core modules
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var jshint = require('gulp-jshint');
var cleanCSS = require('gulp-clean-css');
var less = require('gulp-less');

//including useful modules
var pump = require('pump');
var rename = require('gulp-rename');
var gulpIgnore = require('gulp-ignore');
const stylish = require('jshint-stylish');
var sourcemaps = require('gulp-sourcemaps');

//variables
const jsSrc = './js/*.js';
const sassSrc = './sass/*.scss';
const htmlSrc = './*.html';
const lessSrc = './less/*.less';
const alltasks = [
  'less',
  'sass',
  'lintJs',
  'jsCompress',
  'lintJsInHTML'
];

// param names
const argvfile = process.argv.indexOf("--file");
const argvext = process.argv.indexOf("--ext");
const argvfilename = process.argv.indexOf("--filename");
// params
var file = (argvfile > -1) ? process.argv[argvfile + 1] : null; //file relative to workspaceroot
var ext = (argvext > -1) ? process.argv[argvext + 1].substring(1) : null; //extension of the file
var filename = (argvfilename > -1) ? process.argv[argvfilename + 1] : null; //filename

/*configs*/
const jshintConfig = require('./package').jshintConfig;
jshintConfig.lookup = false;
const cleanCSSConfig = {
  compatibility: 'ie9',
  debug: true
};

gulp.task('lintJs', function (cb) {
  var source = (file === null) ? jsSrc : file;
  pump([
    gulp.src(source),
    gulpIgnore.exclude('*.min.js'),
    gulpIgnore.exclude('**/node_modules/**'),
    gulpIgnore.exclude('**/jquery-*.js'),
    jshint(jshintConfig),
    jshint.reporter(stylish, {
      verbose: true
    })
  ], cb);
});
gulp.task('lintJsInHTML', function (cb) {
  var source = (file === null) ? htmlSrc : file;
  pump([
    gulp.src(source),
    jshint.extract('auto'),
    jshint(jshintConfig),
    jshint.reporter(stylish)
  ], cb);
});
gulp.task('jsCompress', function (cb) {
  var source = (file === null) ? jsSrc : file;
  pump([
    gulp.src(source),
    gulpIgnore.exclude('*.min.js'),
    gulpIgnore.exclude('**/node_modules/**'),
    sourcemaps.init(),
    uglify({
      compress: false
    }),
    sourcemaps.write(),
    rename({
      dirname: ".",
      suffix: ".min",
      extname: ".js"
    }),
    gulp.dest('./js')
  ], cb);
});
gulp.task('sass', function (cb) {
  var source = (file === null) ? sassSrc : file;
  pump([
    gulp.src(source),
    gulpIgnore.exclude('**/node_modules/**'),
    sourcemaps.init(),
    sass({
      outputStyle: 'expanded',
      precision: 8
    }),
    sourcemaps.write(),
    gulp.dest('./css'),
    cleanCSS(cleanCSSConfig, (details) => {
      console.log(details.name + ' before: ' + details.stats.originalSize+' bytes');
      console.log(details.name + '  after: ' + details.stats.minifiedSize+' bytes');
    }),
    sourcemaps.write(),
    rename({
      dirname: ".",
      suffix: ".min",
      extname: ".css"
    }),
    gulp.dest('./css')
  ], cb);
});
gulp.task('less', function (cb) {
  var source = (file === null) ? lessSrc : file;
  pump([
    gulp.src(source),
    gulpIgnore.exclude('**/*.min.less'),
    gulpIgnore.exclude('**/node_modules/**'),
    sourcemaps.init(),
    less(),
    sourcemaps.write(),
    gulp.dest('./css'),
    // gulpIgnore.exclude('*.css.map'),
    cleanCSS(cleanCSSConfig, (details) => {
      console.log(details.name + ' before: ' + details.stats.originalSize+' bytes');
      console.log(details.name + '  after: ' + details.stats.minifiedSize+' bytes');
    }),
    sourcemaps.write(),
    rename({
      dirname: ".",
      suffix: ".min",
      extname: ".css"
    }),
    gulp.dest('./css')
  ], cb);
});

gulp.task('default', function () {
  if (file !== null) {
    if (file === "gulpfile.js" || file === 'srv.js') {
      // do nothing
    } else if (ext === "js") {
      gulp.start("jsCompress");
    } else if ((ext === "scss" || ext === "sass") && filename.substring(0, 1) !== "_") {
      // console.log("Compiling SASS files.\n");
      gulp.start("sass");
    } else if (ext === "html") {
      gulp.start("lintJsInHTML");
    } else if(ext==='less' && filename.substring(0, 1) !== "_"){
      gulp.start('less');
    }
  } else {
    // executes every task
    for (var i = 0; i < alltasks.length; i++) {
      gulp.start(alltasks[i]);
    }
  }
  return;
});
