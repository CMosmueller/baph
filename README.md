# ABADONED

This service is no longer maintained and developed.

# BAPH

**BAPH** – "Browser-based Assistant for Patients Healthcare" supports caregivers in monitoring people in need of special care by providing audio or video stream.

Use a mobile phone, tablet, computer or laptop and connect two of these devices via "BAPH". One device switched on hands-free mode, is positioned in the room of the person to be monitored, so that any incidents can be transmitted over microphone and camera.
The second device is used for checking if everything is okay, using video and audio stream.

**Demo website:** baph.sz-ybbs.ac.at (currently unavailable due to server costs)

## Features

* BAPH works in a browser, so you do not have to install an application.
* Video and audio data can be transmitted according to your wishes.
* Encrypted P2P audio and video streams
* BAPH is free for everybody.
* This service can be used for private and commercial purposes.

### Supported browsers

* Google Chrome and chromium-based browsers
* Some versions of Firefox
* Opera

## TODO

* Visualize audio
* Validate email
* Play sound when the connection is lost
* Improved management system for website

## In consideration

* Use Eslint or Typescript with Tslint
* New/improved design
